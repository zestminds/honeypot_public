sudo apt-get update -y
#install required libs
sudo apt-get install python3-pip python3-twisted python3-venv python3-mysqldb git libpcap-dev mysql-server -y 
sudo apt install -y mongodb
#install virtualenvironment 
#~ pip3 install pipenv
#~ pipenv --python 3.7
pip3 install virtualenv 
python3 -m venv myenv
. ./myenv/bin/activate

pip3 install -r requirements.txt


#clone nu-honeypot
#~ git clone https://bitbucket.org/securenucleon/nu-honeypot.git

#clone p0f
git clone https://bitbucket.org/securenucleon/p0f.git
cd p0f/
#build p0f
./build.sh

#if all ok, run pof - you probably want to change the paths
sudo ./p0f -s /var/run/p0f.sock -d -o /var/log/p0f.log -f ./p0f.fp
cd ..
