# Pop3 honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# import ConfigParser
import logging
import os
import os.path
import sys
import time
import unicodedata
import uuid

from twisted.enterprise import adbapi
from twisted.internet import reactor,protocol
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
from twisted.conch.telnet import TelnetProtocol
from twisted.python import log

import conf


class FakePop3Session(LineReceiver,TelnetProtocol):
    def __init__(self):
        """Initialise the session, state,name and configuration
        argument -- [ self,data ]
        return: object
        """
        self.name = None
        self.delimiter = b'\n'
        self.session = str(uuid.uuid1())  # Dirty. todo.
        self.state = 'AUTHUSER'
        self.conf = conf.ConfigFactory()
    #Changes function names in pop3 file.
    def connectionMade(self):
        """Saves the session to mysql table
        argument -- [ self ]
        return: object
        """
        self.transport.write(
            b'+OK QPOP (version 2.2) at 127.0.0.1 starting.\r\n')#changes to bytes
        #logger.debug(u'%s|New|%s' % (self.session,self.transport.getHost()))
        # ~ self.state = 'AUTHUSER'
        #add port no. of pop3 in database for sessions.
        sessionRecord = {
                "session": ""+self.session+"",
                "ip" : ""+self.transport.getPeer().host+"",
                "protocol" : "pop3",
                "port" : ""+str(self.transport.getHost().port)+"",
                }
        self.conf.log_to_db("pop3Sessions",sessionRecord)
        # ~ self.conf.log_to_db("INSERT INTO `sessions` (session,ip,protocol,port) VALUES('" +
                            # ~ self.session+"','"+self.transport.getPeer().host+"','pop3','"+str(self.transport.getHost().port)+"')")

    def connection_lost(self, reason):
        #logger.debug(u'%s|Closed' % self.session)
        self.l = True

    def lineReceived(self, line):
        """Saves the session commands to mysql table
        argument -- [ self,line ]
        return: object
        """
        #Changes to bytes from string on every transport.write.
        line = line.replace(b"\r", b"")  #Changes to bytes or Remove unneccessary chars        
        command = line.strip().lower()
        cmdsRecord = {
                "session": ""+self.session+"",
                "cmd" : ""+command.decode()+"",
                }
        self.conf.log_to_db("pop3Cmd",cmdsRecord)
        # ~ self.conf.log_to_db(
            # ~ "INSERT INTO `cmds` (session,cmd) VALUES('"+self.session+"','"+command.decode()+"')")
        #string from bytes through decode()
        
        if (command in [b'quit', b'exit']):
            self.transport.write(
                b'+OK Pop server at bilbi.odo.gov signing off.\r\n' )
            #logger.debug(u'%s|Quit' % self.session)

            self.transport.loseConnection()
        elif (command.startswith(b'capa')):
            #logger.debug(u'%s|Capa|%s|%s' % (self.session, self.transport.getHost(),command))
            self.transport.write(b'+OK Capability list follows\r\n')
            self.transport.write(b'TOP\r\n')
            self.transport.write(b'APOP\r\n')
            self.transport.write(b'USER\r\n')
            self.transport.write(b'PASS\r\n')
            self.transport.write(b'STAT\r\n')
            self.transport.write(b'LIST\r\n')
            self.transport.write(b'RETR\r\n')
            self.transport.write(b'DELE\r\n')
            self.transport.write(b'RSET\r\n')
            self.transport.write(b'.\r\n')
        else:
            getattr(self, 'pop3_' + self.state)(command)

    def pop3_AUTHUSER(self, command):
        """Validates the users' password
        argument -- [ self,command ]
        return: object
        """
         #Changes to bytes from string on every transport.write
        if (command.startswith(b'user')):
            #logger.debug(u'%s|User|%s|%s' % (self.session, self.transport.getHost(),command))
            self.user = command[4:].strip()
            self.transport.write(
                b'+OK Password required for %s.\r\n' % command[4:].strip())
            self.state = 'AUTHPASS'
        elif (command.startswith(b'apop') and len(command) > 15):
            #logger.debug(u'%s|apop|%s|%s' % (self.session, self.transport.getHost(),command))
            self.transport.write(b'+OK')
            self.state = 'META'
        else:
            # try :
                        #logger.error(u'%s|Error|%s|%s' % (self.session, self.transport.getHost(),str(cmd)))
            # except :
                        #logger.error(u'%s|Error|%s' % (self.session, self.transport.getHost()))

            self.transport.write(b'-ERR Authentication required.\r\n')

    def pop3_AUTHPASS(self, command):
        """Checks the users' password and
        if exists saves the user details
        argument -- [ self,command ]
        return: object
        """
         #Changes to bytes from string on every transport.write
        if (command.startswith(b'12345')):
            userRecord = {
                "session": ""+self.session+"",
                "password" : ""+command.decode()+""
                }
            self.conf.log_to_db("pop3Users",userRecord)
            # ~ self.conf.log_to_db(
                # ~ "INSERT INTO `users` (session,password) VALUES('"+self.session+"','"+command.decode()+"')")

            self.transport.write(
                b'+OK User has 3 messages (198274 octets).\r\n')
            self.state = 'META'
        else:
          #                      logger.error(u'%s|Error|%s|%s' % (self.session, self.transport.getHost(),command))
            self.transport.write(b'-ERR Password required.\r\n')

    def pop3_META(self, command):
        """saves the session and also checks
        the email state and messages state
        argument -- [ self,command ]
        return: object
        """
         #Changes to bytes from string on every transport.write
        cmdsRecord = {
                "session": ""+self.session+"",
                "cmd" : ""+command.decode()+"",
                }
        self.conf.log_to_db("pop3Cmd",cmdsRecord)
        # ~ self.conf.log_to_db(
            # ~ "INSERT INTO `cmds` (session,cmd) VALUES('"+self.session+"','"+command.decode()+"')")

        if (command.startswith(b'retr') or command.startswith(b'top')):
            self.transport.write(b'-ERR Requested mail does not exist.\r\n')
            #logger.debug(u'%s|RETR|%s|%s' % (self.session, self.transport.getHost(),command))

        elif (command.startswith(b'stat')):
            self.transport.write(b'-ERR Requested mail does not exist.\r\n')
#                                logger.debug(u'%s|STAT|%s|%s' % (self.session, self.transport.getHost(),command))

        elif (command.startswith(b'list')):
            self.transport.write(b'+OK 3 messages:\r\n')
            self.transport.write(b'1 12320\r\n')
            self.transport.write(b'2 23603\r\n')
            self.transport.write(b'3 1240\r\n')
            self.transport.write(b'.\r\n')
 #                      logger.debug(u'%s|LIST|%s|%s' % (self.session, self.transport.getHost(),command))

        elif (command.startswith(b'dele')):
            self.transport.write(b'+OK Message deleted\r\n')
  #                      logger.debug(u'%s|DEL|%s|%8s' % (self.session, self.transport.getHost(),command))

        elif (command.startswith(b'rset')):
            self.transport.write(b'+OK Reset state\r\n')
            self.state = 'AUTHPASS'
#                        logger.debug(u'%s|RSET|%s|%s' % (self.session, self.transport.getHost(),command))

        else:
         #                       logger.error(u'%s|Error|%s' % (self.session, self.transport.getHost()))
            self.transport.write(b'-ERR Invalid command specified.\r\n')

