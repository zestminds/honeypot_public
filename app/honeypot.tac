# Telnet honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from twisted.application import service, internet
from twisted.conch.telnet import TelnetTransport
from twisted.internet.protocol import Factory,Protocol,ServerFactory
from telnet import TelnetPotProtocol
from pop3 import FakePop3Session
from redispot.redisdeploy import RedisServer
from ftp import FTPpot,PotFTPFactory
import logging,sys
from twisted.enterprise import adbapi
import conf

#check if settings enabled
#nucleonEnabled = config.get('Reporting','nucelon')

conf = conf.ConfigFactory()

application = service.Application('NU-HONEYPOT')
serviceCollection = service.IServiceCollection(application)

if conf.telnetEnabled:
    #make a factory protocol and pass the protocol through Telnet Transport to Telnet.py file Class TelnetPotProtocol
    factory = Factory()
    factory.protocol = lambda: TelnetTransport(TelnetPotProtocol)
    
    internet.TCPServer(conf.telnetServerPort, factory).setServiceParent(serviceCollection)
    conf.logger.debug('Telnet Server Starting..')

if conf.pop3Enabled:
    factory = Factory()
    factory.protocol = lambda: TelnetTransport(FakePop3Session) 
    
    internet.TCPServer(conf.pop3ServerPort, factory).setServiceParent(serviceCollection)
    conf.logger.debug('POP3 Server Starting..')
    
if conf.redisEnabled:
    factory = ServerFactory()
    factory.protocol = RedisServer
    
    internet.TCPServer(conf.redisServerPort, factory).setServiceParent(serviceCollection)
    conf.logger.debug('Redis Server Starting..')
	
if conf.ftpEnabled:
    factory = ServerFactory()
    factory.protocol = FTPpot
    
    internet.TCPServer(conf.ftpServerPort, PotFTPFactory()).setServiceParent(serviceCollection)
    conf.logger.debug('FTP Server Starting..')
    

