#!/usr/bin/env python
from twisted.python import log
from twisted.internet.protocol import Protocol, ServerFactory
# ~ import redis_protocol
import sys
import fakeredis
import time
from redispot.redisconfig import rediscommands

### Protocol Implementation of NoPo-Redis Server

global con_count
con_count = 0

global time_elapse,cmd_count
time_elapse = time.time()

cmd_count = 0

class RedisServer(Protocol):
    
    connectionNb = 0

    def __init__(self):
        pass
    
    def connectionMade(self):
        self.connectionNb += 1
        print("New connection: %s from %s"%(format(self.connectionNb),self.transport.getPeer().host))

    #Handling of Client Requests , Data 
    def dataReceived(self, rcvdata):
        cmd_count = 0   
        r = fakeredis.FakeStrictRedis()
        cmd_count = cmd_count + 1
        print("original data:"+str(rcvdata)),
        try:
            data=redis_protocol.decode(rcvdata)
            command=" ".join(redis_protocol.decode(rcvdata))
            print(str(command))
        except:
            data=rcvdata
            command=rcvdata
        print(len(data))
        if command.lower().startswith(b"quit"):
            self.transport.loseConnection()

        else:
            if command.lower().startswith(b"ping") or rcvdata.find(b'PING') == 0:
                snddata = b"+PONG\r\n" 
                self.transport.write(b"+PONG\r\n") 
            elif command.lower() == b"config get *" or rcvdata.find(b'config')==0:
                self.transport.write(rediscommands.parse_config())
            elif command.lower().startswith(b'set') and len(data) == 3:
                print("djvggj")
                if r.set(data[1],data[2]):
                    self.transport.write(b"+OK\r\n")
            elif command.lower().startswith(b'get') and (len(data) == 2 or len(data) == 1):
                if r.get(data[1]):
                    s=r.get(data[1])
                    self.transport.write(b'+"%s"\r\n'%(s))
            elif command.lower().startswith(b'info'):
                diff = round(time.time() - time_elapse) % 60
                print(self.connectionNb)
                self.transport.write(rediscommands.parse_info(diff,self.connectionNb,cmd_count))
            elif command.lower().startswith(b'keys') and (len(data) == 2 or len(data) == 1):
                if r.keys() and (data[1] in r.keys() or data[1] == '*') :
                    keys=r.keys()
                    self.transport.write(rediscommands.encode_keys(keys))
                elif len(r.keys()) == 0:
                    self.transport.write(b"+(empty list or set)\r\n")
                else:
                    self.transport.write(b"-ERR wrong number of arguments for 'keys' command\r\n")
            else:
                self.transport.write(b"-ERR unknown command '%s'\r\n"%(data))
    def connectionLost(self, reason):
        self.connectionNb -= 1
        print("End connection: ", reason.getErrorMessage())
